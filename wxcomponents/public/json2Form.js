  
var app = getApp();
function json2Form(json) {
  
  var str = [];
  for (var p in json) {
    str.push(encodeURIComponent(p) + "=" + encodeURIComponent(json[p]));
  }
  console.log(str.join("&"))
  return str.join("&");
  
}



var socketFun = { 
  uploadImage: function (tempFilePaths, type, params) {
    var app = getApp();
    if (!app.globalData.loginUser) {
      app.globalData.echoErr('用户未登录')
      return
    }
    let param = {
      userId: app.globalData.loginUser.id
    }
    var customIndex = app.globalData.AddClientUrl("/file_uploading.html", param, 'POST')
    wx.uploadFile({
      url: customIndex.url, //接口地址
      filePath: tempFilePaths,
      name: 'file',
      formData: customIndex.params,
      header: { 'content-type': 'multipart/form-data' },
      success: function (res) {
        console.log("===success===", res)
        //do something
        let data = res.data;
        if (typeof (data) == 'string') {
          data = JSON.parse(data)
          console.log("====string====", data)
          if (data.errcode == 0) {
            socketFun.sendMsgFun(data.relateObj.imageUrl, type, params);
          }
        }
      }, fail: function (e) {
        console.log(e)
      }, complete: function (e) {

      }
    })
  },
  sendImgFun: function (type, params) {
    var app = getApp();
    console.log("socket sendImg", type)
    wx.chooseImage({
      count: 9, // 默认9
      sizeType: ['compressed'], // 可以指定是原图还是压缩图，默认二者都有
      sourceType: ['album', 'camera'], // 可以指定来源是相册还是相机，默认二者都有
      success: function (res) {
        console.log("===chooseImage===", res)
        let tempFilePaths = res.tempFilePaths
        for (let i = 0; i < tempFilePaths.length; i++) {
          socketFun.uploadImage(tempFilePaths[i], type, params)
        }
      }
    })
  },
  sendMsgFun: function (msg,type, params) {
    var app= getApp();
    console.log("socket sendMsg", type,app.globalData.loginUser)
    let msgData = '{"viewHandler":"chat","content":"' + msg + '","toUserId":' + params.puid + ',"fromUserId":' + app.globalData.loginUser.platformUser.id + ',"msgType":' + (type || 0) + '}';
    console.log("====msgData==", msgData)
    if (app.globalData.socketTask.readyState == app.globalData.socketTask.OPEN) {
      app.globalData.socketTask.send({
        data: msgData,
        success: () => {
          console.info('客户端发送成功');
        }
      });
    } else {
      console.error('连接已经关闭');
    }
  },
  removeListener:function ( key) {
    var app = getApp();
    if (app.globalData.chatMessageListeners) {
      for (var i = 0; i < app.globalData.chatMessageListeners.length; i++) {
        if (app.globalData.chatMessageListeners[i].key == key) {
          app.globalData.chatMessageListeners.splice(i, 1)
        }
      }
    }
  },
  addListener: function (viewHandler, key, listener, prior) {
    var app = getApp();
    if (!prior) {
      prior = 0;
    }
    if (app.globalData.chatMessageListeners) {
      var replace = false;
      for (var i = 0; i < app.globalData.chatMessageListeners.length; i++) {
        if (app.globalData.chatMessageListeners[i].key == key) {
          app.globalData.chatMessageListeners[i].listener = listener;
          app.globalData.chatMessageListeners[i].prior = prior;
          app.globalData.chatMessageListeners[i].viewHandler = viewHandler;
          replace = true;
        }
      }
      if (!replace) {
        app.globalData.chatMessageListeners.push({ "key": key, "viewHandler": viewHandler, "listener": listener, "prior": prior });
      }
    } else {
      app.globalData.chatMessageListeners = [];
      app.globalData.chatMessageListeners.push({ "key": key, "viewHandler": viewHandler, "listener": listener, "prior": prior });
    }

    app.globalData.chatMessageListeners.sort(function compare(a, b) { return b.prior - a.prior; });
  },
  socketConnect: function () {
    var app = getApp();
    console.log('connect to server!');
    if (app.globalData.socketTask && app.globalData.socketTask.readyState == app.globalData.socketTask.OPEN) {
      console.log("正常连接中~")

    } else {
      let params = { puid: app.globalData.loginUser.platformUser.id }
      let customIndex = app.globalData.AddClientUrl("/connect_chat.socket", params, 'get')
      app.globalData.socketTask = wx.connectSocket({
        url: customIndex.url,
        header: {
          'content-type': app.globalData.header
        },
        success: function (res) {
          console.log("====success====", res)
        },
        fail: function (res) {
          console.log("====fail====", res)
        },
        complete: function (res) {
          console.log("====complete====", res)
        },
      })
      app.globalData.socketTask.onOpen(function () {
        console.log('socket open')
      });
      app.globalData.socketTask.onClose(function () {
        console.log('socket close')
        setTimeout(function () {
          console.log("reconnect socket!!!");
          app.globalData.socketTask = socketFun.socketConnect();
        }, 3000);

      });
      app.globalData.socketTask.onError(function () {
        console.log('socket onError',app.globalData.socketTask)
        try {
          app.globalData.socketTask.close();
          app.globalData.socketTask = null;
        } catch (e) { }
        setTimeout(function () {
          console.log("reconnect socket!!!");
          app.globalData.socketTask = socketFun.socketConnect();
        }, 3000);
      });
      app.globalData.socketTask.onMessage(function (msg) {
        console.log('socket on message', msg, app.globalData.chatMessageListeners)
        if (typeof (msg.data) == 'string') {
          try {
            msg.data = JSON.parse(msg.data)
          } catch (e) { }
        }
        let msgData = msg
        if (app.globalData.chatMessageListeners) {
          for (var i = 0; i < app.globalData.chatMessageListeners.length; i++) {
            if (app.globalData.chatMessageListeners[i].viewHandler == msgData.data.viewHandler) {
              var next = app.globalData.chatMessageListeners[i].listener(msgData);
              if (!next && msgData.isHandler) {
                break;
              }
            } else {

            }

          }
        }
      });
    }
    return app.globalData.socketTask;
  } 
}
export { json2Form, socketFun}