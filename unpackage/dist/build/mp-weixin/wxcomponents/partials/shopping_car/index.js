const app = getApp();
Component({
  properties: {
    data: {
      type: JSON,
      value: 'default value',
    }
  },
  data: {
    shoppingCarData: {},
    cartData:{},
    countGood:0,
    countPrice:0,
    sysWidth:"",
    setting:null,
  },

  ready:function(){
    console.log("=======购物车组件==========",this.data.data)
    this.setData({
      sysWidth: app.globalData.sysWidth,
      setting:app.globalData.setting
    });
    app.globalData.addCarChangeNotify(this);
    this.getCart();
  },
  methods: {
    carChangeNotify:function(data){
      console.log("====car change=====", data)
      let that=this;
      if (data =='clear'){
        this.setData({
          pushItem: [],
          countGood: 0,
          countPrice: 0
        })
      }
      else{
        this.getCart();
      }
    },
    getCart: function (type) {
      var customIndex = app.globalData.AddClientUrl("/get_shopping_car_list_item.html")
      var that = this
      wx.request({
        url: customIndex.url,
        header: app.globalData.header,
        success: function (res) {
          console.log("加载的数据", res.data)
          if (res.data.errcode == '10001') {
            that.data.cartData = null
            that.setData({
              cartData: that.data.cartData
            })
            // app.globalData.loadLogin()
          } else if (res.data.result.errcode == '-1') {
            that.data.cartData = null
            that.setData({
              cartData: that.data.cartData
            })
            app.globalData.echoErr(res.data.result.errMsg)
          } else {
            if (!res.data.result || res.data.result.length == 0) {
              that.data.cartData = null
              that.setData({
                cartData: that.data.cartData
              })
            } else if (res.data.result.errcode) {
              that.data.cartData = null
              that.setData({
                cartData: that.data.cartData
              })
            } else {
              console.log("======success====")
              that.data.cartData = res.data.result;
              that.setData({
                cartData: that.data.cartData,
              })
              console.log("======successcartData====", that.data.cartData)
            }
            that.showPrice()
          }
        },
        fail: function (res) {

        }
      })
    },
    showPrice: function () {
      let that=this;
      let resultData = app.globalData.showPrice(that.data.cartData);
      console.log("===========resultData==========", resultData)
      this.setData({
        pushItem: resultData.pushItem||[],
        countGood: resultData.countGood,
        countPrice: resultData.countPrice
      })
    },
    tolinkUrl: function (event) {
      console.log(event.currentTarget.dataset.link)
      app.globalData.linkEvent(event.currentTarget.dataset.link);
    }
  },
})