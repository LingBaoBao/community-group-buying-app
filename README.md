﻿# 微信小程序
## 微信小程序源码：社区团购小程序。

#### 项目介绍

	此项目是一套可用于在小程序客户端上发布各种食品食材等商品，客户购买后在相应社区点自提的社区团购系统

#### 功能介绍

	1. 可在后台编辑添加食品食材
	2. 可在后台编辑添加食品食材的各大分类
	3. 后台可录入每个门店自提点的信息
	4. 后台有活动营销功能
	5. 其他基本功能

    - Tip 更换页面，在app.json里面设置路径即可
	
### QQ交流群 — 24934459
### 公司官网 - http://www.fz33.net  官网
### 公司其他项目案例

***共享充电宝：https://gitee.com/sansanC/sharing-power-bank-app***

多门店派单：https://gitee.com/sansanC/multiple-stores-send-single-applet 

在线课程：https://gitee.com/sansanC/online-course-applet 

健身馆：https://gitee.com/sansanC/gym-app 

派单：https://gitee.com/sansanC/dispatch-applet 

场馆预定：https://gitee.com/sansanC/venue-booking-procedures 

社区团购小程序：https://gitee.com/sansanC/community-group-buying-app 

早餐线上预订：https://gitee.com/sansanC/breakfast-subscription-applet 

相册资源存储https://gitee.com/sansanC/photo-album-applet 

美容美发：https://gitee.com/sansanC/beauty-salon-small-program

商城小程序：https://gitee.com/sansanC/wechatApp

按摩小程序：https://gitee.com/sansanC/massage-applet

### 管理后台效果图（部分图）

|登录入口：http://www.sansancloud.com/manager/#/login|试用账号：yanshi 密码：yanshi123
|:----|:----:|
|![file-list](http://image1.sansancloud.com/xianhua/2021_3/26/16/43/5_455.jpg)|![file-list](http://image1.sansancloud.com/xianhua/2021_3/26/16/43/6_10.jpg)
|![file-list](http://image1.sansancloud.com/xianhua/2021_3/26/16/43/5_571.jpg)|![file-list](http://image1.sansancloud.com/xianhua/2021_3/26/16/43/5_480.jpg)
### 效果图---扫码查看（部分图片）

|往下扫码预览|往下扫码预览|往下扫码预览|
|:----:|:----:|:----:|
|![file-list](http://image1.sansancloud.com/xianhua/2021_3/26/16/43/6_84.jpg)|![file-list](http://image1.sansancloud.com/xianhua/2021_3/26/16/43/6_105.jpg)|![file-list](http://image1.sansancloud.com/xianhua/2021_3/26/16/43/6_172.jpg)
|![file-list](http://image1.sansancloud.com/xianhua/2021_3/26/16/43/5_624.jpg)|![file-list](http://image1.sansancloud.com/xianhua/2021_3/26/16/43/5_390.jpg)|![file-list](http://image1.sansancloud.com/xianhua/2021_3/26/16/43/5_657.jpg)
### 公司资质

|省高薪证书|国高薪证书|
|:----:|:----:|
|![file-list](http://image1.sansancloud.com/xianhua/2021_3/24/15/0/8_826.jpg)|![file-list](http://image1.sansancloud.com/xianhua/2021_3/24/15/0/6_933.jpg)
    

![file-list](http://image1.sansancloud.com/xianhua/2021_3/26/16/46/34_453.jpg)